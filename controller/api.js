const db = require('../util/database')

exports.deleteSal = (req,res,next)=>{
  const id = req.body.id
  db.execute('DELETE FROM salary WHERE id = ?',[id],(err,result,field)=>{
      if (!err) {
          res.status(200).json({msg:'deleted'})
      }else{
          res.status(400).json({err:err})
    
      }
  })
}

exports.updateSal = (req, res, next) => {
  const id = req.body.id
  const ename = req.body.ename
  const month = req.body.month
  const year = req.body.year
  const amout = req.body.amout
  const gd = req.body.gd

  db.execute(
    'UPDATE salary SET emp_id = (SELECT id FROM employee WHERE name = ?),month=?,year=?,amout=?,generated_date=? WHERE id = ?',
    [ename, month, year, amout, gd, id],
    (err, result, field) => {
      if (!err) {
        res.status(200).json({ msg: 'updated' })
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}

exports.addSal = (req, res, next) => {
  const ename = req.body.ename
  const month = req.body.month
  const year = req.body.year
  const amout = req.body.amout
  const gd = req.body.gd

  db.execute(
    'INSERT INTO salary(emp_id,month,year,amout,generated_date) VALUES( (SELECT id FROM employee WHERE name = ?),?,?,?,?)',
    [ename, month, year, amout, gd],
    (err, result, field) => {
      if (!err) {
        res.status(200).json({ msg: 'added' })
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}

exports.deleteEmp = (req, res, next) => {
  const id = req.body.id

  db.execute(
    'DELETE FROM salary where emp_id = ?',
    [id],
    (err, reslut, field) => {
      if (!err) {
        db.execute(
          'DELETE FROM employee WHERE id = ?',
          [id],
          (err, reslut, field) => {
            if (!err) {
              res.status(200).json({ msg: 'deleted' })
            } else {
              res.status(400).json({ err: err })
            }
          }
        )
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}

exports.updateEmp = (req, res, next) => {
  const id = req.body.id
  const name = req.body.name
  const empno = req.body.empno
  const dname = req.body.dname
  const jdate = req.body.jdate
  const edate = req.body.edate
  db.execute(
    'UPDATE employee SET name = ? ,emp_no = ?, dept_no = (SELECT id FROM department WHERE name = ?),join_date = ?,end_date = ? WHERE id = ? ',
    [name, empno, dname, jdate, edate, id],
    (err, reslut, field) => {
      if (!err) {
        res.status(200).json({ msg: 'updated' })
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}

exports.addEmpy = (req, res, next) => {
  const dname = req.body.dname
  const name = req.body.name
  const jdate = req.body.jdate
  const edate = req.body.edate
  const eno = req.body.eno

  db.execute(
    'INSERT INTO employee(dept_no,name,join_date,end_date,emp_no) VALUES ( (SELECT id FROM department WHERE name = ?) , ? , ? , ? ,?)',
    [dname, name, jdate, edate, eno],
    (err, reslut, field) => {
      res.status(200).json({ msg: 'sus' })
    }
  )
}

exports.delteDept = (req, res, next) => {
  const name = req.body.name

  db.execute(
    'DELETE FROM salary WHERE emp_id = (SELECT id FROM employee WHERE dept_no = (SELECT id FROM department WHERE name = ?))',
    [name],
    (err, result, filed) => {
      if (!err) {
        db.execute(
          'DELETE FROM employee WHERE dept_no = (SELECT id from department WHERE name = ?)',
          [name],
          (err, result, field) => {
            if (!err) {
              db.execute(
                'DELETE FROM department WHERE name = ?',
                [name],
                (err, result, field) => {
                  if (!err) {
                    res.status(200).json({ msg: 'delete sus' })
                  } else {
                    res.status(400).json({ msg: err })
                  }
                }
              )
            } else {
              res.status(400).json({ msg: err })
            }
          }
        )
      } else {
        res.status(400).json({ msg: err })
      }
    }
  )
}

exports.addDept = (req, res, next) => {
  const name = req.body.name
  const date = req.body.date

  db.execute(
    'INSERT INTO department (name,created_date) VALUES (?,?)',
    [name, date],
    (err, result, field) => {
      if (!err) {
        res.status(200).json({ msg: 'sus' })
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}

exports.updateDept = (req, res, next) => {
  const name = req.body.name
  const created_date = req.body.cdate
  const id = req.body.id
  db.execute(
    'UPDATE department SET name = ? ,created_date =? WHERE id = ?',
    [name, created_date, id],
    (err, result, field) => {
      if (!err) {
        res.status(200).json({ msg: 'sus' })
      } else {
        res.status(400).json({ err: err })
      }
    }
  )
}
