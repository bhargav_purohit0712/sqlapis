const express = require('express')

const router = express.Router()

const apiController = require('../controller/api')

//update department
router.put('/updateDept', apiController.updateDept)

//add department
router.post('/deparment', apiController.addDept)

//delete department
router.delete('/deleteDept', apiController.delteDept)

//add employee
router.post('/employee', apiController.addEmpy)

//update employee
router.put('/updateEmp', apiController.updateEmp)

//delete employee
router.delete('/deleteEmp', apiController.deleteEmp)

//add sal
router.post('/salary', apiController.addSal)

//update sal
router.put('/updateSal', apiController.updateSal)

//delete sal
router.delete('/deleteSal', apiController.deleteEmp)

module.exports = router
