const express = require('express')
const api = require('./routes/api')
const app = express()
const bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json())

app.use('/api', api)

app.listen(3000)
